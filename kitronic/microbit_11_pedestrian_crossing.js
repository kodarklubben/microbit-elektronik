/**
 * https://www.kitronik.co.uk/blog/inventors-kit-experiment-11
 */

input.onButtonPressed(Button.A, () => {
  pins.digitalWritePin(DigitalPin.P12, 0)
  pins.digitalWritePin(DigitalPin.P8, 1)
  basic.pause(1000)
  pins.digitalWritePin(DigitalPin.P8, 0)
  pins.digitalWritePin(DigitalPin.P1, 1)
  basic.pause(1000)
  pins.digitalWritePin(DigitalPin.P2, 0)
  pins.digitalWritePin(DigitalPin.P16, 1)
  basic.pause(1000)
  for (let i = 0; i < 8; i++) {
      music.playTone(262, music.beat(BeatFraction.Whole))
      basic.pause(200)
  }
  pins.digitalWritePin(DigitalPin.P2, 1)
  pins.digitalWritePin(DigitalPin.P16, 0)
  basic.pause(1000)
  pins.digitalWritePin(DigitalPin.P1, 1)
  pins.digitalWritePin(DigitalPin.P8, 1)
  basic.pause(1000)
  pins.digitalWritePin(DigitalPin.P1, 0)
  pins.digitalWritePin(DigitalPin.P8, 0)
  pins.digitalWritePin(DigitalPin.P12, 1)
})
pins.digitalWritePin(DigitalPin.P12, 1)
pins.digitalWritePin(DigitalPin.P2, 1)
