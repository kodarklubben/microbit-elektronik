/**
 * https://www.kitronik.co.uk/blog/experiment-4-using-a-transistor-to-drive-a-motor/
 */

let Duty = 0
Duty = 0
basic.forever(() => {
    while (Duty < 1023) {
        pins.analogWritePin(AnalogPin.P0, Duty)
        Duty = Duty + 1
        basic.pause(10)
    }
    while (Duty > 0) {
        pins.analogWritePin(AnalogPin.P0, Duty)
        Duty = Duty - 1
        basic.pause(10)
    }
})
