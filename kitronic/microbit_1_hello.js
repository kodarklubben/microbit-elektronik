/**
 * https://www.kitronik.co.uk/blog/inventors-kit-experiment-1-help
 */

input.onButtonPressed(Button.A, () => {
  basic.showLeds(`
      . # . # .
      . # . # .
      . . . . .
      # . . . #
      . # # # .
      `)
})
input.onButtonPressed(Button.B, () => {
  basic.showString("Hello world")
})
