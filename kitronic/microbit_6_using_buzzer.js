/**
 * https://www.kitronik.co.uk/blog/inventors-kit-experiment-6-help
 */

input.onButtonPressed(Button.A, () => {
  music.playTone(400, 500)
})
input.onButtonPressed(Button.B, () => {
  music.playTone(262, music.beat(BeatFraction.Whole))
})
input.onButtonPressed(Button.AB, () => {
  music.beginMelody(music.builtInMelody(Melodies.Dadadadum), MelodyOptions.Once)
})
