/**
 * https://www.kitronik.co.uk/blog/inventors-kit-experiment-3-further-help
 */

let Light_State = 0
input.onPinPressed(TouchPin.P0, () => {
    if (Light_State == 0) {
        Light_State = 1
    } else {
        Light_State = 0
    }
})
basic.forever(() => {
    if (Light_State == 1) {
        pins.analogWritePin(AnalogPin.P2, pins.analogReadPin(AnalogPin.P1))
    } else {
        pins.digitalWritePin(DigitalPin.P2, 0)
    }
})
