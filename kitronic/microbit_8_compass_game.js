/**
 * https://www.kitronik.co.uk/blog/inventors-kit-experiment-8-further-help
 */

let goal = 0
let img: Image = null
let difference = 0
let degrees = 0
input.onButtonPressed(Button.A, () => {
    if (difference < 15) {
        basic.showString("Winner")
        goal = Math.random(360)
    } else {
        basic.showString("Try Again")
        goal = Math.random(360)
    }
})
input.calibrateCompass()
img = images.createImage(`
    . # . # .
    . # . # .
    . . . . .
    # . . . #
    . # # # .
    `)
img.showImage(0)
goal = Math.random(360)
basic.forever(() => {
    degrees = input.compassHeading()
    difference = Math.abs(goal - degrees)
    pins.digitalWritePin(DigitalPin.P0, 1)
    basic.pause(difference * 5)
    pins.digitalWritePin(DigitalPin.P0, 0)
    basic.pause(difference * 5)
})
