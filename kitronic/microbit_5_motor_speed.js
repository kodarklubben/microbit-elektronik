/**
 * https://www.kitronik.co.uk/blog/inventors-kit-experiment-5-further-help
 */

basic.forever(() => {
  pins.analogWritePin(AnalogPin.P0, Math.abs(input.acceleration(Dimension.Y)))
})
