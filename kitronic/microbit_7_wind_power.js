/**
 * https://www.kitronik.co.uk/blog/experiment-7-wind-power
 */

let Highest = 0
let value = 0
input.onButtonPressed(Button.A, () => {
    basic.showNumber(Highest)
})
basic.forever(() => {
    value = pins.analogReadPin(AnalogPin.P0)
    if (value > Highest) {
        Highest = value
    }
})
