/**
 * https://www.kitronik.co.uk/blog/experiment-2-using-an-ldr-analog-inputs/
 */

let Light = 0
basic.forever(() => {
    Light = pins.analogReadPin(AnalogPin.P0)
    if (Light > 512) {
        basic.showLeds(`
            # . # . #
            . # # # .
            # # # # #
            . # # # .
            # . # . #
            `)
    } else {
        basic.showLeds(`
            # # # . .
            . # # # .
            . . # # .
            . # # # .
            # # # . .
            `)
    }
})
