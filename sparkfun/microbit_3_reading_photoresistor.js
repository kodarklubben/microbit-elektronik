let item = 0
let calibrationVal = 0
calibrationVal = pins.analogReadPin(AnalogPin.P0)
basic.forever(() => {
    item = pins.analogReadPin(AnalogPin.P0)
    if (item < calibrationVal - 50) {
        pins.digitalWritePin(DigitalPin.P16, 1)
    } else {
        pins.digitalWritePin(DigitalPin.P16, 0)
    }
})
