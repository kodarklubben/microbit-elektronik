/**
 * Needs external package in MakeCode:
 * https://github.com/sparkfun/pxt-tmp36
 */

let degreesC = 0
basic.forever(() => {
    degreesC = tmp36.temp(AnalogPin.P2, tmp36Type.C)
    basic.showNumber(degreesC)
    basic.pause(500)
})
