music.beginMelody(music.builtInMelody(Melodies.Prelude), MelodyOptions.Once)
basic.forever(() => {
    if (pins.digitalReadPin(DigitalPin.P15) == 1) {
        music.playTone(262, music.beat(BeatFraction.Breve))
    } else if (pins.digitalReadPin(DigitalPin.P16) == 1) {
        music.beginMelody(music.builtInMelody(Melodies.Birthday), MelodyOptions.Once)
    }
})
