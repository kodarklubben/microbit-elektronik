let PotentiometerVal = 0
basic.forever(() => {
    PotentiometerVal = pins.analogReadPin(AnalogPin.P2)
    pins.analogWritePin(AnalogPin.P0, PotentiometerVal)
})
