let Angle = 0
let direction = 0
direction = 1
Angle = 0
pins.servoWritePin(AnalogPin.P0, Angle)
basic.forever(() => {
    Angle += direction
    pins.servoWritePin(AnalogPin.P0, Angle)
    if (Angle >= 180 || Angle <= 0) {
        direction = direction * -1
    }
})
