let mapValue = 0
let item = 0
pins.servoWritePin(AnalogPin.P0, 90)
basic.forever(() => {
    item = input.acceleration(Dimension.X)
    mapValue = pins.map(
    item,
    -1023,
    1023,
    15,
    165
    )
    pins.servoWritePin(AnalogPin.P0, mapValue)
})
