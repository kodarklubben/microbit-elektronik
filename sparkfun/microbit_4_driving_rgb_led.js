let blueVal = 0
let greenVal = 0
let redVal = 0
input.onButtonPressed(Button.A, () => {
    redVal = 0
    greenVal = 255
    blueVal = 0
})
input.onButtonPressed(Button.B, () => {
    redVal = 0
    greenVal = 0
    blueVal = 255
})
input.onButtonPressed(Button.AB, () => {
    redVal = Math.random(256)
    greenVal = Math.random(256)
    blueVal = Math.random(256)
})
redVal = 255
greenVal = 0
blueVal = 0
basic.forever(() => {
    pins.analogWritePin(AnalogPin.P0, redVal)
    pins.analogWritePin(AnalogPin.P1, greenVal)
    pins.analogWritePin(AnalogPin.P2, blueVal)
})
