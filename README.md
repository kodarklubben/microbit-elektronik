# Fortsättningskurs i programmering med micro:bit och elektronik

Experimenten förutsätter ett "Inventors Kit" från antingen SparkFun eller Kitronik:

- https://www.sparkfun.com/products/14542
- https://www.kitronik.co.uk/5603-inventors-kit-for-the-bbc-microbit.html

